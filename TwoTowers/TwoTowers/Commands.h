#pragma once

extern bool Open;
namespace TwoTowers {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Commands
	/// </summary>
	public ref class Commands : public System::Windows::Forms::Form
	{
	public:
		Commands(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Commands()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button6;
	protected:

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(9, 9);
			this->button1->Margin = System::Windows::Forms::Padding(0);
			this->button1->MaximumSize = System::Drawing::Size(266, 40);
			this->button1->MinimumSize = System::Drawing::Size(266, 40);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(266, 40);
			this->button1->TabIndex = 0;
			this->button1->Text = L"�������� ��������";
			this->button1->UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button2->Location = System::Drawing::Point(9, 60);
			this->button2->Margin = System::Windows::Forms::Padding(0);
			this->button2->MaximumSize = System::Drawing::Size(266, 40);
			this->button2->MinimumSize = System::Drawing::Size(266, 40);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(266, 40);
			this->button2->TabIndex = 1;
			this->button2->Text = L"��������� ��������";
			this->button2->UseVisualStyleBackColor = true;
			// 
			// button3
			// 
			this->button3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button3->Location = System::Drawing::Point(9, 162);
			this->button3->Margin = System::Windows::Forms::Padding(0);
			this->button3->MaximumSize = System::Drawing::Size(266, 40);
			this->button3->MinimumSize = System::Drawing::Size(266, 40);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(266, 40);
			this->button3->TabIndex = 1;
			this->button3->Text = L"��������� �������� ��������";
			this->button3->UseVisualStyleBackColor = true;
			// 
			// button5
			// 
			this->button5->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button5->Location = System::Drawing::Point(9, 412);
			this->button5->Margin = System::Windows::Forms::Padding(0);
			this->button5->MaximumSize = System::Drawing::Size(266, 40);
			this->button5->MinimumSize = System::Drawing::Size(266, 40);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(266, 40);
			this->button5->TabIndex = 1;
			this->button5->Text = L"�������";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Commands::button5_Click);
			// 
			// button4
			// 
			this->button4->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button4->Location = System::Drawing::Point(9, 111);
			this->button4->Margin = System::Windows::Forms::Padding(0);
			this->button4->MaximumSize = System::Drawing::Size(266, 40);
			this->button4->MinimumSize = System::Drawing::Size(266, 40);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(266, 40);
			this->button4->TabIndex = 2;
			this->button4->Text = L"��������� �������";
			this->button4->UseVisualStyleBackColor = true;
			// 
			// button6
			// 
			this->button6->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button6->Location = System::Drawing::Point(9, 213);
			this->button6->Margin = System::Windows::Forms::Padding(0);
			this->button6->MaximumSize = System::Drawing::Size(266, 40);
			this->button6->MinimumSize = System::Drawing::Size(266, 40);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(266, 40);
			this->button6->TabIndex = 3;
			this->button6->Text = L"���������";
			this->button6->UseVisualStyleBackColor = true;
			// 
			// Commands
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 461);
			this->ControlBox = false;
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(300, 500);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(300, 500);
			this->Name = L"Commands";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"�������� ��������";
			this->Load += gcnew System::EventHandler(this, &Commands::Commands_Load);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Commands_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
				 Open = false;
				 Close();
	}
};
}
