#pragma once
#include "Commands.h"
#include "Options.h"
#include "time.h"
#include "iostream"
#include "fstream"

//����� ��������� ���������
const int N = 9;

//����� ��������� ���������� ����������
bool Open = false, OpenOpt = false;
int countOfMount = 0;
int countOfRuins = 0;
int production[2] = {}; //��������� 1 � 2 �������
int army[2] = {}; //����� 1 � 2 �������
int action;

//����� ��������� ���������� ���������
struct BoxCharactreristic
{
	bool isMount;
	int worker;
	int tower;
	int barrack;
	int player;
	bool net;
	bool isRuin;
	int defense;
};

struct Coordinates
{
	int i, j;
};

//����� ��������� ���������� �������� ��������
BoxCharactreristic map[N][N];

//����� ��������� ���������� ������������
void generateMap()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			map[i][j].isMount = false;
			map[i][j].worker = 0;
			map[i][j].tower = 0;
			map[i][j].barrack = 0;
			map[i][j].player = 0;
			map[i][j].net = false;
			map[i][j].isRuin = false;
			map[i][j].defense = 0;
		}
	}
}

void generateMount()
{
	int string, column;
	srand(time(NULL));
	map[N/2][N/2].isMount = true;
	string = rand() % N;
	column = rand() % N;
	for (int i = 1; i < countOfMount; i++)
	{
		while (map[string][column].isMount)
		{
			string = rand() % N;
			column = rand() % N;
		}
		map[string][column].isMount = true;
	}
}

void generateRuins()
{
	Coordinates* CoordinatesOfRuins = new Coordinates[countOfRuins];

	for (int k = 0; k < countOfRuins; k++)
	{
		do
		{
			CoordinatesOfRuins[k].i = rand() % (N-1);
			CoordinatesOfRuins[k].j = rand() % (N-1);
		} while ((map[CoordinatesOfRuins[k].i][CoordinatesOfRuins[k].j].isMount) || (map[CoordinatesOfRuins[k].i][CoordinatesOfRuins[k].j].isRuin));
		map[CoordinatesOfRuins[k].i][CoordinatesOfRuins[k].j].isRuin = true;
	}
}

namespace TwoTowers {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;
	


	/// <summary>
	/// ������ ��� Map
	/// </summary>
	public ref class Map : public System::Windows::Forms::Form
	{
	public:
		Map(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Map()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  cell_1_1;
	private: System::Windows::Forms::PictureBox^  cell_1_2;
	private: System::Windows::Forms::PictureBox^  cell_1_3;
	private: System::Windows::Forms::PictureBox^  cell_1_4;
	private: System::Windows::Forms::PictureBox^  cell_1_5;
	private: System::Windows::Forms::PictureBox^  cell_1_6;
	private: System::Windows::Forms::PictureBox^  cell_1_7;
	private: System::Windows::Forms::PictureBox^  cell_1_8;
	private: System::Windows::Forms::PictureBox^  cell_1_9;
	private: System::Windows::Forms::PictureBox^  cell_2_1;
	private: System::Windows::Forms::PictureBox^  cell_2_2;
	private: System::Windows::Forms::PictureBox^  cell_2_4;
	private: System::Windows::Forms::PictureBox^  cell_2_5;
	private: System::Windows::Forms::PictureBox^  cell_2_7;
	private: System::Windows::Forms::PictureBox^  cell_2_8;
	protected:

	protected:
	private: System::Windows::Forms::PictureBox^  cell_2_3;
	private: System::Windows::Forms::PictureBox^  cell_2_6;
	private: System::Windows::Forms::PictureBox^  cell_2_9;
	private: System::Windows::Forms::PictureBox^  cell_3_1;
	private: System::Windows::Forms::PictureBox^  cell_3_2;
	private: System::Windows::Forms::PictureBox^  cell_3_4;
	private: System::Windows::Forms::PictureBox^  cell_3_5;
	private: System::Windows::Forms::PictureBox^  cell_3_7;
	private: System::Windows::Forms::PictureBox^  cell_3_8;
	private: System::Windows::Forms::PictureBox^  cell_3_3;
	private: System::Windows::Forms::PictureBox^  cell_3_6;
	private: System::Windows::Forms::PictureBox^  cell_3_9;
	private: System::Windows::Forms::PictureBox^  cell_4_1;
	private: System::Windows::Forms::PictureBox^  cell_4_2;
	private: System::Windows::Forms::PictureBox^  cell_4_4;

	private: System::Windows::Forms::PictureBox^  cell_4_7;
	private: System::Windows::Forms::PictureBox^  cell_4_8;
	private: System::Windows::Forms::PictureBox^  cell_4_3;
	private: System::Windows::Forms::PictureBox^  cell_4_6;
	private: System::Windows::Forms::PictureBox^  cell_4_9;
	private: System::Windows::Forms::PictureBox^  cell_5_1;
	private: System::Windows::Forms::PictureBox^  cell_5_2;
	private: System::Windows::Forms::PictureBox^  cell_5_4;
	private: System::Windows::Forms::PictureBox^  cell_5_5;
	private: System::Windows::Forms::PictureBox^  cell_5_7;
	private: System::Windows::Forms::PictureBox^  cell_5_8;
private: System::Windows::Forms::PictureBox^  cell_5_3;
private: System::Windows::Forms::PictureBox^  cell_5_6;
private: System::Windows::Forms::PictureBox^  cell_5_9;
private: System::Windows::Forms::PictureBox^  cell_6_1;
private: System::Windows::Forms::PictureBox^  cell_6_2;
private: System::Windows::Forms::PictureBox^  cell_6_4;
private: System::Windows::Forms::PictureBox^  cell_6_5;
private: System::Windows::Forms::PictureBox^  cell_6_7;
private: System::Windows::Forms::PictureBox^  cell_6_8;
private: System::Windows::Forms::PictureBox^  cell_6_3;
private: System::Windows::Forms::PictureBox^  cell_6_6;
private: System::Windows::Forms::PictureBox^  cell_6_9;
private: System::Windows::Forms::PictureBox^  cell_7_1;
private: System::Windows::Forms::PictureBox^  cell_7_2;
private: System::Windows::Forms::PictureBox^  cell_7_4;
private: System::Windows::Forms::PictureBox^  cell_7_5;
private: System::Windows::Forms::PictureBox^  cell_7_7;
private: System::Windows::Forms::PictureBox^  cell_7_8;
private: System::Windows::Forms::PictureBox^  cell_7_3;
private: System::Windows::Forms::PictureBox^  cell_7_6;
private: System::Windows::Forms::PictureBox^  cell_7_9;
private: System::Windows::Forms::PictureBox^  cell_8_1;
private: System::Windows::Forms::PictureBox^  cell_8_2;
private: System::Windows::Forms::PictureBox^  cell_8_4;
private: System::Windows::Forms::PictureBox^  cell_8_5;
private: System::Windows::Forms::PictureBox^  cell_8_7;
private: System::Windows::Forms::PictureBox^  cell_8_8;
private: System::Windows::Forms::PictureBox^  cell_8_3;
private: System::Windows::Forms::PictureBox^  cell_8_6;
private: System::Windows::Forms::PictureBox^  cell_8_9;
private: System::Windows::Forms::PictureBox^  cell_9_1;
private: System::Windows::Forms::PictureBox^  cell_9_2;
private: System::Windows::Forms::PictureBox^  cell_9_4;
private: System::Windows::Forms::PictureBox^  cell_9_5;
private: System::Windows::Forms::PictureBox^  cell_9_7;
private: System::Windows::Forms::PictureBox^  cell_9_8;
private: System::Windows::Forms::PictureBox^  cell_9_3;
private: System::Windows::Forms::PictureBox^  cell_9_6;
private: System::Windows::Forms::PictureBox^  cell_9_9;
private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::Label^  label2;
private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::Button^  button1;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::Label^  label5;
private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::Label^  label8;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::Button^  button3;
private: System::Windows::Forms::Label^  label10;
private: System::Windows::Forms::Label^  label11;
private: System::Windows::Forms::Label^  label12;
private: System::Windows::Forms::Button^  button4;
private: System::Windows::Forms::PictureBox^  cell_4_5;
private: System::Windows::Forms::Label^  label13;
private: System::Windows::Forms::Label^  label14;
private: System::Windows::Forms::Label^  label15;
private: System::Windows::Forms::Label^  label16;
private: System::Windows::Forms::Label^  label17;
private: System::Windows::Forms::Label^  label18;
private: System::Windows::Forms::Label^  label19;
private: System::Windows::Forms::Label^  label20;



	protected:

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->cell_1_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_1_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_2_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_3_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_4_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_5_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_6_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_7_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_8_9 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_1 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_2 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_4 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_5 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_7 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_8 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_3 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_6 = (gcnew System::Windows::Forms::PictureBox());
			this->cell_9_9 = (gcnew System::Windows::Forms::PictureBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->cell_4_5 = (gcnew System::Windows::Forms::PictureBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_5))->BeginInit();
			this->SuspendLayout();
			// 
			// cell_1_1
			// 
			this->cell_1_1->Location = System::Drawing::Point(182, 32);
			this->cell_1_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_1->Name = L"cell_1_1";
			this->cell_1_1->Size = System::Drawing::Size(50, 50);
			this->cell_1_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_1->TabIndex = 0;
			this->cell_1_1->TabStop = false;
			this->cell_1_1->Click += gcnew System::EventHandler(this, &Map::cell_1_1_Click);
			// 
			// cell_1_2
			// 
			this->cell_1_2->Location = System::Drawing::Point(241, 32);
			this->cell_1_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_2->Name = L"cell_1_2";
			this->cell_1_2->Size = System::Drawing::Size(50, 50);
			this->cell_1_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_2->TabIndex = 1;
			this->cell_1_2->TabStop = false;
			this->cell_1_2->Click += gcnew System::EventHandler(this, &Map::cell_1_2_Click);
			// 
			// cell_1_3
			// 
			this->cell_1_3->Location = System::Drawing::Point(300, 32);
			this->cell_1_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_3->Name = L"cell_1_3";
			this->cell_1_3->Size = System::Drawing::Size(50, 50);
			this->cell_1_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_3->TabIndex = 2;
			this->cell_1_3->TabStop = false;
			this->cell_1_3->Click += gcnew System::EventHandler(this, &Map::cell_1_3_Click);
			// 
			// cell_1_4
			// 
			this->cell_1_4->Location = System::Drawing::Point(359, 32);
			this->cell_1_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_4->Name = L"cell_1_4";
			this->cell_1_4->Size = System::Drawing::Size(50, 50);
			this->cell_1_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_4->TabIndex = 0;
			this->cell_1_4->TabStop = false;
			this->cell_1_4->Click += gcnew System::EventHandler(this, &Map::cell_1_4_Click);
			// 
			// cell_1_5
			// 
			this->cell_1_5->Location = System::Drawing::Point(418, 32);
			this->cell_1_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_5->Name = L"cell_1_5";
			this->cell_1_5->Size = System::Drawing::Size(50, 50);
			this->cell_1_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_5->TabIndex = 1;
			this->cell_1_5->TabStop = false;
			this->cell_1_5->Click += gcnew System::EventHandler(this, &Map::cell_1_5_Click);
			// 
			// cell_1_6
			// 
			this->cell_1_6->Location = System::Drawing::Point(477, 32);
			this->cell_1_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_6->Name = L"cell_1_6";
			this->cell_1_6->Size = System::Drawing::Size(50, 50);
			this->cell_1_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_6->TabIndex = 2;
			this->cell_1_6->TabStop = false;
			this->cell_1_6->Click += gcnew System::EventHandler(this, &Map::cell_1_6_Click);
			// 
			// cell_1_7
			// 
			this->cell_1_7->Location = System::Drawing::Point(536, 32);
			this->cell_1_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_7->Name = L"cell_1_7";
			this->cell_1_7->Size = System::Drawing::Size(50, 50);
			this->cell_1_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_7->TabIndex = 0;
			this->cell_1_7->TabStop = false;
			this->cell_1_7->Click += gcnew System::EventHandler(this, &Map::cell_1_7_Click);
			// 
			// cell_1_8
			// 
			this->cell_1_8->Location = System::Drawing::Point(595, 32);
			this->cell_1_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_8->Name = L"cell_1_8";
			this->cell_1_8->Size = System::Drawing::Size(50, 50);
			this->cell_1_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_8->TabIndex = 1;
			this->cell_1_8->TabStop = false;
			this->cell_1_8->Click += gcnew System::EventHandler(this, &Map::cell_1_8_Click);
			// 
			// cell_1_9
			// 
			this->cell_1_9->Location = System::Drawing::Point(654, 32);
			this->cell_1_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_1_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_1_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_1_9->Name = L"cell_1_9";
			this->cell_1_9->Size = System::Drawing::Size(50, 50);
			this->cell_1_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_1_9->TabIndex = 2;
			this->cell_1_9->TabStop = false;
			this->cell_1_9->Click += gcnew System::EventHandler(this, &Map::cell_1_9_Click);
			// 
			// cell_2_1
			// 
			this->cell_2_1->Location = System::Drawing::Point(182, 91);
			this->cell_2_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_1->Name = L"cell_2_1";
			this->cell_2_1->Size = System::Drawing::Size(50, 50);
			this->cell_2_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_1->TabIndex = 0;
			this->cell_2_1->TabStop = false;
			this->cell_2_1->Click += gcnew System::EventHandler(this, &Map::cell_2_1_Click);
			// 
			// cell_2_2
			// 
			this->cell_2_2->Location = System::Drawing::Point(241, 91);
			this->cell_2_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_2->Name = L"cell_2_2";
			this->cell_2_2->Size = System::Drawing::Size(50, 50);
			this->cell_2_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_2->TabIndex = 1;
			this->cell_2_2->TabStop = false;
			this->cell_2_2->Click += gcnew System::EventHandler(this, &Map::cell_2_2_Click);
			// 
			// cell_2_4
			// 
			this->cell_2_4->Location = System::Drawing::Point(359, 91);
			this->cell_2_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_4->Name = L"cell_2_4";
			this->cell_2_4->Size = System::Drawing::Size(50, 50);
			this->cell_2_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_4->TabIndex = 0;
			this->cell_2_4->TabStop = false;
			this->cell_2_4->Click += gcnew System::EventHandler(this, &Map::cell_2_4_Click);
			// 
			// cell_2_5
			// 
			this->cell_2_5->Location = System::Drawing::Point(418, 91);
			this->cell_2_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_5->Name = L"cell_2_5";
			this->cell_2_5->Size = System::Drawing::Size(50, 50);
			this->cell_2_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_5->TabIndex = 1;
			this->cell_2_5->TabStop = false;
			this->cell_2_5->Click += gcnew System::EventHandler(this, &Map::cell_2_5_Click);
			// 
			// cell_2_7
			// 
			this->cell_2_7->Location = System::Drawing::Point(536, 91);
			this->cell_2_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_7->Name = L"cell_2_7";
			this->cell_2_7->Size = System::Drawing::Size(50, 50);
			this->cell_2_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_7->TabIndex = 0;
			this->cell_2_7->TabStop = false;
			this->cell_2_7->Click += gcnew System::EventHandler(this, &Map::cell_2_7_Click);
			// 
			// cell_2_8
			// 
			this->cell_2_8->Location = System::Drawing::Point(595, 91);
			this->cell_2_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_8->Name = L"cell_2_8";
			this->cell_2_8->Size = System::Drawing::Size(50, 50);
			this->cell_2_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_8->TabIndex = 1;
			this->cell_2_8->TabStop = false;
			this->cell_2_8->Click += gcnew System::EventHandler(this, &Map::cell_2_8_Click);
			// 
			// cell_2_3
			// 
			this->cell_2_3->Location = System::Drawing::Point(300, 91);
			this->cell_2_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_3->Name = L"cell_2_3";
			this->cell_2_3->Size = System::Drawing::Size(50, 50);
			this->cell_2_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_3->TabIndex = 2;
			this->cell_2_3->TabStop = false;
			this->cell_2_3->Click += gcnew System::EventHandler(this, &Map::cell_2_3_Click);
			// 
			// cell_2_6
			// 
			this->cell_2_6->Location = System::Drawing::Point(477, 91);
			this->cell_2_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_6->Name = L"cell_2_6";
			this->cell_2_6->Size = System::Drawing::Size(50, 50);
			this->cell_2_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_6->TabIndex = 2;
			this->cell_2_6->TabStop = false;
			this->cell_2_6->Click += gcnew System::EventHandler(this, &Map::cell_2_6_Click);
			// 
			// cell_2_9
			// 
			this->cell_2_9->Location = System::Drawing::Point(654, 91);
			this->cell_2_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_2_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_2_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_2_9->Name = L"cell_2_9";
			this->cell_2_9->Size = System::Drawing::Size(50, 50);
			this->cell_2_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_2_9->TabIndex = 2;
			this->cell_2_9->TabStop = false;
			this->cell_2_9->Click += gcnew System::EventHandler(this, &Map::cell_2_9_Click);
			// 
			// cell_3_1
			// 
			this->cell_3_1->Location = System::Drawing::Point(182, 150);
			this->cell_3_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_1->Name = L"cell_3_1";
			this->cell_3_1->Size = System::Drawing::Size(50, 50);
			this->cell_3_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_1->TabIndex = 0;
			this->cell_3_1->TabStop = false;
			this->cell_3_1->Click += gcnew System::EventHandler(this, &Map::cell_3_1_Click);
			// 
			// cell_3_2
			// 
			this->cell_3_2->Location = System::Drawing::Point(241, 150);
			this->cell_3_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_2->Name = L"cell_3_2";
			this->cell_3_2->Size = System::Drawing::Size(50, 50);
			this->cell_3_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_2->TabIndex = 1;
			this->cell_3_2->TabStop = false;
			this->cell_3_2->Click += gcnew System::EventHandler(this, &Map::cell_3_2_Click);
			// 
			// cell_3_4
			// 
			this->cell_3_4->Location = System::Drawing::Point(359, 150);
			this->cell_3_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_4->Name = L"cell_3_4";
			this->cell_3_4->Size = System::Drawing::Size(50, 50);
			this->cell_3_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_4->TabIndex = 0;
			this->cell_3_4->TabStop = false;
			this->cell_3_4->Click += gcnew System::EventHandler(this, &Map::cell_3_4_Click);
			// 
			// cell_3_5
			// 
			this->cell_3_5->Location = System::Drawing::Point(418, 150);
			this->cell_3_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_5->Name = L"cell_3_5";
			this->cell_3_5->Size = System::Drawing::Size(50, 50);
			this->cell_3_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_5->TabIndex = 1;
			this->cell_3_5->TabStop = false;
			this->cell_3_5->Click += gcnew System::EventHandler(this, &Map::cell_3_5_Click);
			// 
			// cell_3_7
			// 
			this->cell_3_7->Location = System::Drawing::Point(536, 150);
			this->cell_3_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_7->Name = L"cell_3_7";
			this->cell_3_7->Size = System::Drawing::Size(50, 50);
			this->cell_3_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_7->TabIndex = 0;
			this->cell_3_7->TabStop = false;
			this->cell_3_7->Click += gcnew System::EventHandler(this, &Map::cell_3_7_Click);
			// 
			// cell_3_8
			// 
			this->cell_3_8->Location = System::Drawing::Point(595, 150);
			this->cell_3_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_8->Name = L"cell_3_8";
			this->cell_3_8->Size = System::Drawing::Size(50, 50);
			this->cell_3_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_8->TabIndex = 1;
			this->cell_3_8->TabStop = false;
			this->cell_3_8->Click += gcnew System::EventHandler(this, &Map::cell_3_8_Click);
			// 
			// cell_3_3
			// 
			this->cell_3_3->Location = System::Drawing::Point(300, 150);
			this->cell_3_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_3->Name = L"cell_3_3";
			this->cell_3_3->Size = System::Drawing::Size(50, 50);
			this->cell_3_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_3->TabIndex = 2;
			this->cell_3_3->TabStop = false;
			this->cell_3_3->Click += gcnew System::EventHandler(this, &Map::cell_3_3_Click);
			// 
			// cell_3_6
			// 
			this->cell_3_6->Location = System::Drawing::Point(477, 150);
			this->cell_3_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_6->Name = L"cell_3_6";
			this->cell_3_6->Size = System::Drawing::Size(50, 50);
			this->cell_3_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_6->TabIndex = 2;
			this->cell_3_6->TabStop = false;
			this->cell_3_6->Click += gcnew System::EventHandler(this, &Map::cell_3_6_Click);
			// 
			// cell_3_9
			// 
			this->cell_3_9->Location = System::Drawing::Point(654, 150);
			this->cell_3_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_3_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_3_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_3_9->Name = L"cell_3_9";
			this->cell_3_9->Size = System::Drawing::Size(50, 50);
			this->cell_3_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_3_9->TabIndex = 2;
			this->cell_3_9->TabStop = false;
			this->cell_3_9->Click += gcnew System::EventHandler(this, &Map::cell_3_9_Click);
			// 
			// cell_4_1
			// 
			this->cell_4_1->Location = System::Drawing::Point(182, 209);
			this->cell_4_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_1->Name = L"cell_4_1";
			this->cell_4_1->Size = System::Drawing::Size(50, 50);
			this->cell_4_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_1->TabIndex = 0;
			this->cell_4_1->TabStop = false;
			this->cell_4_1->Click += gcnew System::EventHandler(this, &Map::cell_4_1_Click);
			// 
			// cell_4_2
			// 
			this->cell_4_2->Location = System::Drawing::Point(241, 209);
			this->cell_4_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_2->Name = L"cell_4_2";
			this->cell_4_2->Size = System::Drawing::Size(50, 50);
			this->cell_4_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_2->TabIndex = 1;
			this->cell_4_2->TabStop = false;
			this->cell_4_2->Click += gcnew System::EventHandler(this, &Map::cell_4_2_Click);
			// 
			// cell_4_4
			// 
			this->cell_4_4->Location = System::Drawing::Point(359, 209);
			this->cell_4_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_4->Name = L"cell_4_4";
			this->cell_4_4->Size = System::Drawing::Size(50, 50);
			this->cell_4_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_4->TabIndex = 0;
			this->cell_4_4->TabStop = false;
			this->cell_4_4->Click += gcnew System::EventHandler(this, &Map::cell_4_4_Click);
			// 
			// cell_4_7
			// 
			this->cell_4_7->Location = System::Drawing::Point(536, 209);
			this->cell_4_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_7->Name = L"cell_4_7";
			this->cell_4_7->Size = System::Drawing::Size(50, 50);
			this->cell_4_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_7->TabIndex = 0;
			this->cell_4_7->TabStop = false;
			this->cell_4_7->Click += gcnew System::EventHandler(this, &Map::cell_4_7_Click);
			// 
			// cell_4_8
			// 
			this->cell_4_8->Location = System::Drawing::Point(595, 209);
			this->cell_4_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_8->Name = L"cell_4_8";
			this->cell_4_8->Size = System::Drawing::Size(50, 50);
			this->cell_4_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_8->TabIndex = 1;
			this->cell_4_8->TabStop = false;
			this->cell_4_8->Click += gcnew System::EventHandler(this, &Map::cell_4_8_Click);
			// 
			// cell_4_3
			// 
			this->cell_4_3->Location = System::Drawing::Point(300, 209);
			this->cell_4_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_3->Name = L"cell_4_3";
			this->cell_4_3->Size = System::Drawing::Size(50, 50);
			this->cell_4_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_3->TabIndex = 2;
			this->cell_4_3->TabStop = false;
			this->cell_4_3->Click += gcnew System::EventHandler(this, &Map::cell_4_3_Click);
			// 
			// cell_4_6
			// 
			this->cell_4_6->Location = System::Drawing::Point(477, 209);
			this->cell_4_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_6->Name = L"cell_4_6";
			this->cell_4_6->Size = System::Drawing::Size(50, 50);
			this->cell_4_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_6->TabIndex = 2;
			this->cell_4_6->TabStop = false;
			this->cell_4_6->Click += gcnew System::EventHandler(this, &Map::cell_4_6_Click);
			// 
			// cell_4_9
			// 
			this->cell_4_9->Location = System::Drawing::Point(654, 209);
			this->cell_4_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_9->Name = L"cell_4_9";
			this->cell_4_9->Size = System::Drawing::Size(50, 50);
			this->cell_4_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_9->TabIndex = 2;
			this->cell_4_9->TabStop = false;
			this->cell_4_9->Click += gcnew System::EventHandler(this, &Map::cell_4_9_Click);
			// 
			// cell_5_1
			// 
			this->cell_5_1->Location = System::Drawing::Point(182, 268);
			this->cell_5_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_1->Name = L"cell_5_1";
			this->cell_5_1->Size = System::Drawing::Size(50, 50);
			this->cell_5_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_1->TabIndex = 0;
			this->cell_5_1->TabStop = false;
			this->cell_5_1->Click += gcnew System::EventHandler(this, &Map::cell_5_1_Click);
			// 
			// cell_5_2
			// 
			this->cell_5_2->Location = System::Drawing::Point(241, 268);
			this->cell_5_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_2->Name = L"cell_5_2";
			this->cell_5_2->Size = System::Drawing::Size(50, 50);
			this->cell_5_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_2->TabIndex = 1;
			this->cell_5_2->TabStop = false;
			this->cell_5_2->Click += gcnew System::EventHandler(this, &Map::cell_5_2_Click);
			// 
			// cell_5_4
			// 
			this->cell_5_4->Location = System::Drawing::Point(359, 268);
			this->cell_5_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_4->Name = L"cell_5_4";
			this->cell_5_4->Size = System::Drawing::Size(50, 50);
			this->cell_5_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_4->TabIndex = 0;
			this->cell_5_4->TabStop = false;
			this->cell_5_4->Click += gcnew System::EventHandler(this, &Map::cell_5_4_Click);
			// 
			// cell_5_5
			// 
			this->cell_5_5->Location = System::Drawing::Point(418, 268);
			this->cell_5_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_5->Name = L"cell_5_5";
			this->cell_5_5->Size = System::Drawing::Size(50, 50);
			this->cell_5_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_5->TabIndex = 1;
			this->cell_5_5->TabStop = false;
			this->cell_5_5->Click += gcnew System::EventHandler(this, &Map::cell_5_5_Click);
			// 
			// cell_5_7
			// 
			this->cell_5_7->Location = System::Drawing::Point(536, 268);
			this->cell_5_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_7->Name = L"cell_5_7";
			this->cell_5_7->Size = System::Drawing::Size(50, 50);
			this->cell_5_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_7->TabIndex = 0;
			this->cell_5_7->TabStop = false;
			this->cell_5_7->Click += gcnew System::EventHandler(this, &Map::cell_5_7_Click);
			// 
			// cell_5_8
			// 
			this->cell_5_8->Location = System::Drawing::Point(595, 268);
			this->cell_5_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_8->Name = L"cell_5_8";
			this->cell_5_8->Size = System::Drawing::Size(50, 50);
			this->cell_5_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_8->TabIndex = 1;
			this->cell_5_8->TabStop = false;
			this->cell_5_8->Click += gcnew System::EventHandler(this, &Map::cell_5_8_Click);
			// 
			// cell_5_3
			// 
			this->cell_5_3->Location = System::Drawing::Point(300, 268);
			this->cell_5_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_3->Name = L"cell_5_3";
			this->cell_5_3->Size = System::Drawing::Size(50, 50);
			this->cell_5_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_3->TabIndex = 2;
			this->cell_5_3->TabStop = false;
			this->cell_5_3->Click += gcnew System::EventHandler(this, &Map::cell_5_3_Click);
			// 
			// cell_5_6
			// 
			this->cell_5_6->Location = System::Drawing::Point(477, 268);
			this->cell_5_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_6->Name = L"cell_5_6";
			this->cell_5_6->Size = System::Drawing::Size(50, 50);
			this->cell_5_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_6->TabIndex = 2;
			this->cell_5_6->TabStop = false;
			this->cell_5_6->Click += gcnew System::EventHandler(this, &Map::cell_5_6_Click);
			// 
			// cell_5_9
			// 
			this->cell_5_9->Location = System::Drawing::Point(654, 268);
			this->cell_5_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_5_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_5_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_5_9->Name = L"cell_5_9";
			this->cell_5_9->Size = System::Drawing::Size(50, 50);
			this->cell_5_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_5_9->TabIndex = 2;
			this->cell_5_9->TabStop = false;
			this->cell_5_9->Click += gcnew System::EventHandler(this, &Map::cell_5_9_Click);
			// 
			// cell_6_1
			// 
			this->cell_6_1->Location = System::Drawing::Point(182, 327);
			this->cell_6_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_1->Name = L"cell_6_1";
			this->cell_6_1->Size = System::Drawing::Size(50, 50);
			this->cell_6_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_1->TabIndex = 0;
			this->cell_6_1->TabStop = false;
			this->cell_6_1->Click += gcnew System::EventHandler(this, &Map::cell_6_1_Click);
			// 
			// cell_6_2
			// 
			this->cell_6_2->Location = System::Drawing::Point(241, 327);
			this->cell_6_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_2->Name = L"cell_6_2";
			this->cell_6_2->Size = System::Drawing::Size(50, 50);
			this->cell_6_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_2->TabIndex = 1;
			this->cell_6_2->TabStop = false;
			this->cell_6_2->Click += gcnew System::EventHandler(this, &Map::cell_6_2_Click);
			// 
			// cell_6_4
			// 
			this->cell_6_4->Location = System::Drawing::Point(359, 327);
			this->cell_6_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_4->Name = L"cell_6_4";
			this->cell_6_4->Size = System::Drawing::Size(50, 50);
			this->cell_6_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_4->TabIndex = 0;
			this->cell_6_4->TabStop = false;
			this->cell_6_4->Click += gcnew System::EventHandler(this, &Map::cell_6_4_Click);
			// 
			// cell_6_5
			// 
			this->cell_6_5->Location = System::Drawing::Point(418, 327);
			this->cell_6_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_5->Name = L"cell_6_5";
			this->cell_6_5->Size = System::Drawing::Size(50, 50);
			this->cell_6_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_5->TabIndex = 1;
			this->cell_6_5->TabStop = false;
			this->cell_6_5->Click += gcnew System::EventHandler(this, &Map::cell_6_5_Click);
			// 
			// cell_6_7
			// 
			this->cell_6_7->Location = System::Drawing::Point(536, 327);
			this->cell_6_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_7->Name = L"cell_6_7";
			this->cell_6_7->Size = System::Drawing::Size(50, 50);
			this->cell_6_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_7->TabIndex = 0;
			this->cell_6_7->TabStop = false;
			this->cell_6_7->Click += gcnew System::EventHandler(this, &Map::cell_6_7_Click);
			// 
			// cell_6_8
			// 
			this->cell_6_8->Location = System::Drawing::Point(595, 327);
			this->cell_6_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_8->Name = L"cell_6_8";
			this->cell_6_8->Size = System::Drawing::Size(50, 50);
			this->cell_6_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->cell_6_8->TabIndex = 1;
			this->cell_6_8->TabStop = false;
			this->cell_6_8->Click += gcnew System::EventHandler(this, &Map::cell_6_8_Click);
			// 
			// cell_6_3
			// 
			this->cell_6_3->Location = System::Drawing::Point(300, 327);
			this->cell_6_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_3->Name = L"cell_6_3";
			this->cell_6_3->Size = System::Drawing::Size(50, 50);
			this->cell_6_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_3->TabIndex = 2;
			this->cell_6_3->TabStop = false;
			this->cell_6_3->Click += gcnew System::EventHandler(this, &Map::cell_6_3_Click);
			// 
			// cell_6_6
			// 
			this->cell_6_6->Location = System::Drawing::Point(477, 327);
			this->cell_6_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_6->Name = L"cell_6_6";
			this->cell_6_6->Size = System::Drawing::Size(50, 50);
			this->cell_6_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_6->TabIndex = 2;
			this->cell_6_6->TabStop = false;
			this->cell_6_6->Click += gcnew System::EventHandler(this, &Map::cell_6_6_Click);
			// 
			// cell_6_9
			// 
			this->cell_6_9->Location = System::Drawing::Point(654, 327);
			this->cell_6_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_6_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_6_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_6_9->Name = L"cell_6_9";
			this->cell_6_9->Size = System::Drawing::Size(50, 50);
			this->cell_6_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_6_9->TabIndex = 2;
			this->cell_6_9->TabStop = false;
			this->cell_6_9->Click += gcnew System::EventHandler(this, &Map::cell_6_9_Click);
			// 
			// cell_7_1
			// 
			this->cell_7_1->Location = System::Drawing::Point(182, 386);
			this->cell_7_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_1->Name = L"cell_7_1";
			this->cell_7_1->Size = System::Drawing::Size(50, 50);
			this->cell_7_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_1->TabIndex = 0;
			this->cell_7_1->TabStop = false;
			this->cell_7_1->Click += gcnew System::EventHandler(this, &Map::cell_7_1_Click);
			// 
			// cell_7_2
			// 
			this->cell_7_2->Location = System::Drawing::Point(241, 386);
			this->cell_7_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_2->Name = L"cell_7_2";
			this->cell_7_2->Size = System::Drawing::Size(50, 50);
			this->cell_7_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_2->TabIndex = 1;
			this->cell_7_2->TabStop = false;
			this->cell_7_2->Click += gcnew System::EventHandler(this, &Map::cell_7_2_Click);
			// 
			// cell_7_4
			// 
			this->cell_7_4->Location = System::Drawing::Point(359, 386);
			this->cell_7_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_4->Name = L"cell_7_4";
			this->cell_7_4->Size = System::Drawing::Size(50, 50);
			this->cell_7_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_4->TabIndex = 0;
			this->cell_7_4->TabStop = false;
			this->cell_7_4->Click += gcnew System::EventHandler(this, &Map::cell_7_4_Click);
			// 
			// cell_7_5
			// 
			this->cell_7_5->Location = System::Drawing::Point(418, 386);
			this->cell_7_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_5->Name = L"cell_7_5";
			this->cell_7_5->Size = System::Drawing::Size(50, 50);
			this->cell_7_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_5->TabIndex = 1;
			this->cell_7_5->TabStop = false;
			this->cell_7_5->Click += gcnew System::EventHandler(this, &Map::cell_7_5_Click);
			// 
			// cell_7_7
			// 
			this->cell_7_7->Location = System::Drawing::Point(536, 386);
			this->cell_7_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_7->Name = L"cell_7_7";
			this->cell_7_7->Size = System::Drawing::Size(50, 50);
			this->cell_7_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_7->TabIndex = 0;
			this->cell_7_7->TabStop = false;
			this->cell_7_7->Click += gcnew System::EventHandler(this, &Map::cell_7_7_Click);
			// 
			// cell_7_8
			// 
			this->cell_7_8->Location = System::Drawing::Point(595, 386);
			this->cell_7_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_8->Name = L"cell_7_8";
			this->cell_7_8->Size = System::Drawing::Size(50, 50);
			this->cell_7_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_8->TabIndex = 1;
			this->cell_7_8->TabStop = false;
			this->cell_7_8->Click += gcnew System::EventHandler(this, &Map::cell_7_8_Click);
			// 
			// cell_7_3
			// 
			this->cell_7_3->Location = System::Drawing::Point(300, 386);
			this->cell_7_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_3->Name = L"cell_7_3";
			this->cell_7_3->Size = System::Drawing::Size(50, 50);
			this->cell_7_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_3->TabIndex = 2;
			this->cell_7_3->TabStop = false;
			this->cell_7_3->Click += gcnew System::EventHandler(this, &Map::cell_7_3_Click);
			// 
			// cell_7_6
			// 
			this->cell_7_6->Location = System::Drawing::Point(477, 386);
			this->cell_7_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_6->Name = L"cell_7_6";
			this->cell_7_6->Size = System::Drawing::Size(50, 50);
			this->cell_7_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_6->TabIndex = 2;
			this->cell_7_6->TabStop = false;
			this->cell_7_6->Click += gcnew System::EventHandler(this, &Map::cell_7_6_Click);
			// 
			// cell_7_9
			// 
			this->cell_7_9->Location = System::Drawing::Point(654, 386);
			this->cell_7_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_7_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_7_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_7_9->Name = L"cell_7_9";
			this->cell_7_9->Size = System::Drawing::Size(50, 50);
			this->cell_7_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_7_9->TabIndex = 2;
			this->cell_7_9->TabStop = false;
			this->cell_7_9->Click += gcnew System::EventHandler(this, &Map::cell_7_9_Click);
			// 
			// cell_8_1
			// 
			this->cell_8_1->Location = System::Drawing::Point(182, 445);
			this->cell_8_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_1->Name = L"cell_8_1";
			this->cell_8_1->Size = System::Drawing::Size(50, 50);
			this->cell_8_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_1->TabIndex = 0;
			this->cell_8_1->TabStop = false;
			this->cell_8_1->Click += gcnew System::EventHandler(this, &Map::cell_8_1_Click);
			// 
			// cell_8_2
			// 
			this->cell_8_2->Location = System::Drawing::Point(241, 445);
			this->cell_8_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_2->Name = L"cell_8_2";
			this->cell_8_2->Size = System::Drawing::Size(50, 50);
			this->cell_8_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_2->TabIndex = 1;
			this->cell_8_2->TabStop = false;
			this->cell_8_2->Click += gcnew System::EventHandler(this, &Map::cell_8_2_Click);
			// 
			// cell_8_4
			// 
			this->cell_8_4->Location = System::Drawing::Point(359, 445);
			this->cell_8_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_4->Name = L"cell_8_4";
			this->cell_8_4->Size = System::Drawing::Size(50, 50);
			this->cell_8_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_4->TabIndex = 0;
			this->cell_8_4->TabStop = false;
			this->cell_8_4->Click += gcnew System::EventHandler(this, &Map::cell_8_4_Click);
			// 
			// cell_8_5
			// 
			this->cell_8_5->Location = System::Drawing::Point(418, 445);
			this->cell_8_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_5->Name = L"cell_8_5";
			this->cell_8_5->Size = System::Drawing::Size(50, 50);
			this->cell_8_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_5->TabIndex = 1;
			this->cell_8_5->TabStop = false;
			this->cell_8_5->Click += gcnew System::EventHandler(this, &Map::cell_8_5_Click);
			// 
			// cell_8_7
			// 
			this->cell_8_7->Location = System::Drawing::Point(536, 445);
			this->cell_8_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_7->Name = L"cell_8_7";
			this->cell_8_7->Size = System::Drawing::Size(50, 50);
			this->cell_8_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_7->TabIndex = 0;
			this->cell_8_7->TabStop = false;
			this->cell_8_7->Click += gcnew System::EventHandler(this, &Map::cell_8_7_Click);
			// 
			// cell_8_8
			// 
			this->cell_8_8->Location = System::Drawing::Point(595, 445);
			this->cell_8_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_8->Name = L"cell_8_8";
			this->cell_8_8->Size = System::Drawing::Size(50, 50);
			this->cell_8_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_8->TabIndex = 1;
			this->cell_8_8->TabStop = false;
			this->cell_8_8->Click += gcnew System::EventHandler(this, &Map::cell_8_8_Click);
			// 
			// cell_8_3
			// 
			this->cell_8_3->Location = System::Drawing::Point(300, 445);
			this->cell_8_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_3->Name = L"cell_8_3";
			this->cell_8_3->Size = System::Drawing::Size(50, 50);
			this->cell_8_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_3->TabIndex = 2;
			this->cell_8_3->TabStop = false;
			this->cell_8_3->Click += gcnew System::EventHandler(this, &Map::cell_8_3_Click);
			// 
			// cell_8_6
			// 
			this->cell_8_6->Location = System::Drawing::Point(477, 445);
			this->cell_8_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_6->Name = L"cell_8_6";
			this->cell_8_6->Size = System::Drawing::Size(50, 50);
			this->cell_8_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_6->TabIndex = 2;
			this->cell_8_6->TabStop = false;
			this->cell_8_6->Click += gcnew System::EventHandler(this, &Map::cell_8_6_Click);
			// 
			// cell_8_9
			// 
			this->cell_8_9->Location = System::Drawing::Point(654, 445);
			this->cell_8_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_8_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_8_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_8_9->Name = L"cell_8_9";
			this->cell_8_9->Size = System::Drawing::Size(50, 50);
			this->cell_8_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_8_9->TabIndex = 2;
			this->cell_8_9->TabStop = false;
			this->cell_8_9->Click += gcnew System::EventHandler(this, &Map::cell_8_9_Click);
			// 
			// cell_9_1
			// 
			this->cell_9_1->Location = System::Drawing::Point(182, 504);
			this->cell_9_1->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_1->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_1->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_1->Name = L"cell_9_1";
			this->cell_9_1->Size = System::Drawing::Size(50, 50);
			this->cell_9_1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_1->TabIndex = 0;
			this->cell_9_1->TabStop = false;
			this->cell_9_1->Click += gcnew System::EventHandler(this, &Map::cell_9_1_Click);
			// 
			// cell_9_2
			// 
			this->cell_9_2->Location = System::Drawing::Point(241, 504);
			this->cell_9_2->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_2->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_2->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_2->Name = L"cell_9_2";
			this->cell_9_2->Size = System::Drawing::Size(50, 50);
			this->cell_9_2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_2->TabIndex = 1;
			this->cell_9_2->TabStop = false;
			this->cell_9_2->Click += gcnew System::EventHandler(this, &Map::cell_9_2_Click);
			// 
			// cell_9_4
			// 
			this->cell_9_4->Location = System::Drawing::Point(359, 504);
			this->cell_9_4->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_4->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_4->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_4->Name = L"cell_9_4";
			this->cell_9_4->Size = System::Drawing::Size(50, 50);
			this->cell_9_4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_4->TabIndex = 0;
			this->cell_9_4->TabStop = false;
			this->cell_9_4->Click += gcnew System::EventHandler(this, &Map::cell_9_4_Click);
			// 
			// cell_9_5
			// 
			this->cell_9_5->Location = System::Drawing::Point(418, 504);
			this->cell_9_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_5->Name = L"cell_9_5";
			this->cell_9_5->Size = System::Drawing::Size(50, 50);
			this->cell_9_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_5->TabIndex = 1;
			this->cell_9_5->TabStop = false;
			this->cell_9_5->Click += gcnew System::EventHandler(this, &Map::cell_9_5_Click);
			// 
			// cell_9_7
			// 
			this->cell_9_7->Location = System::Drawing::Point(536, 504);
			this->cell_9_7->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_7->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_7->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_7->Name = L"cell_9_7";
			this->cell_9_7->Size = System::Drawing::Size(50, 50);
			this->cell_9_7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_7->TabIndex = 0;
			this->cell_9_7->TabStop = false;
			this->cell_9_7->Click += gcnew System::EventHandler(this, &Map::cell_9_7_Click);
			// 
			// cell_9_8
			// 
			this->cell_9_8->Location = System::Drawing::Point(595, 504);
			this->cell_9_8->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_8->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_8->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_8->Name = L"cell_9_8";
			this->cell_9_8->Size = System::Drawing::Size(50, 50);
			this->cell_9_8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_8->TabIndex = 1;
			this->cell_9_8->TabStop = false;
			this->cell_9_8->Click += gcnew System::EventHandler(this, &Map::cell_9_8_Click);
			// 
			// cell_9_3
			// 
			this->cell_9_3->Location = System::Drawing::Point(300, 504);
			this->cell_9_3->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_3->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_3->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_3->Name = L"cell_9_3";
			this->cell_9_3->Size = System::Drawing::Size(50, 50);
			this->cell_9_3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_3->TabIndex = 2;
			this->cell_9_3->TabStop = false;
			this->cell_9_3->Click += gcnew System::EventHandler(this, &Map::cell_9_3_Click);
			// 
			// cell_9_6
			// 
			this->cell_9_6->Location = System::Drawing::Point(477, 504);
			this->cell_9_6->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_6->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_6->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_6->Name = L"cell_9_6";
			this->cell_9_6->Size = System::Drawing::Size(50, 50);
			this->cell_9_6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_6->TabIndex = 2;
			this->cell_9_6->TabStop = false;
			this->cell_9_6->Click += gcnew System::EventHandler(this, &Map::cell_9_6_Click);
			// 
			// cell_9_9
			// 
			this->cell_9_9->Location = System::Drawing::Point(654, 504);
			this->cell_9_9->Margin = System::Windows::Forms::Padding(0);
			this->cell_9_9->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_9_9->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_9_9->Name = L"cell_9_9";
			this->cell_9_9->Size = System::Drawing::Size(50, 50);
			this->cell_9_9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_9_9->TabIndex = 2;
			this->cell_9_9->TabStop = false;
			this->cell_9_9->Click += gcnew System::EventHandler(this, &Map::cell_9_9_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(51, 42);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(82, 25);
			this->label1->TabIndex = 3;
			this->label1->Text = L"����� 1";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(759, 42);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(82, 25);
			this->label2->TabIndex = 4;
			this->label2->Text = L"����� 2";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label3->Location = System::Drawing::Point(345, 4);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(99, 20);
			this->label3->TabIndex = 5;
			this->label3->Text = L"��� ������:";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->Location = System::Drawing::Point(451, 563);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(120, 40);
			this->button1->TabIndex = 6;
			this->button1->Text = L"��������� ���";
			this->button1->UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button2->Location = System::Drawing::Point(585, 563);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(120, 40);
			this->button2->TabIndex = 7;
			this->button2->Text = L"�������";
			this->button2->UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label4->Location = System::Drawing::Point(12, 105);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(116, 18);
			this->label4->TabIndex = 8;
			this->label4->Text = L"������������:";
			this->label4->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label5->Location = System::Drawing::Point(12, 193);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(60, 18);
			this->label5->TabIndex = 9;
			this->label5->Text = L"�����:";
			this->label5->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Cursor = System::Windows::Forms::Cursors::Default;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label6->Location = System::Drawing::Point(12, 281);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(90, 36);
			this->label6->TabIndex = 10;
			this->label6->Text = L"������ ��� \r\n���������:";
			this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Cursor = System::Windows::Forms::Cursors::Default;
			this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label7->Location = System::Drawing::Point(723, 281);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(90, 36);
			this->label7->TabIndex = 13;
			this->label7->Text = L"������ ��� \r\n���������:";
			this->label7->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label8->Location = System::Drawing::Point(723, 193);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(60, 18);
			this->label8->TabIndex = 12;
			this->label8->Text = L"�����:";
			this->label8->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label9->Location = System::Drawing::Point(723, 105);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(116, 18);
			this->label9->TabIndex = 11;
			this->label9->Text = L"������������:";
			this->label9->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button3->Location = System::Drawing::Point(316, 563);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(120, 40);
			this->button3->TabIndex = 14;
			this->button3->Text = L"���������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Map::button3_Click);
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Cursor = System::Windows::Forms::Cursors::Default;
			this->label10->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label10->Location = System::Drawing::Point(12, 369);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(125, 18);
			this->label10->TabIndex = 15;
			this->label10->Text = L"���� ���������:";
			this->label10->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Cursor = System::Windows::Forms::Cursors::Default;
			this->label11->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 4, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Millimeter,
				static_cast<System::Byte>(204)));
			this->label11->Location = System::Drawing::Point(723, 369);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(125, 18);
			this->label11->TabIndex = 16;
			this->label11->Text = L"���� ���������:";
			this->label11->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label12->Location = System::Drawing::Point(487, 4);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(44, 20);
			this->label12->TabIndex = 17;
			this->label12->Text = L"���:";
			this->label12->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// button4
			// 
			this->button4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button4->Location = System::Drawing::Point(182, 563);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(120, 40);
			this->button4->TabIndex = 18;
			this->button4->Text = L"����� ����";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Map::button4_Click);
			// 
			// cell_4_5
			// 
			this->cell_4_5->Location = System::Drawing::Point(418, 209);
			this->cell_4_5->Margin = System::Windows::Forms::Padding(0);
			this->cell_4_5->MaximumSize = System::Drawing::Size(50, 50);
			this->cell_4_5->MinimumSize = System::Drawing::Size(50, 50);
			this->cell_4_5->Name = L"cell_4_5";
			this->cell_4_5->Size = System::Drawing::Size(50, 50);
			this->cell_4_5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->cell_4_5->TabIndex = 1;
			this->cell_4_5->TabStop = false;
			this->cell_4_5->Click += gcnew System::EventHandler(this, &Map::cell_4_5_Click);
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(134, 109);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(13, 13);
			this->label13->TabIndex = 19;
			this->label13->Text = L"0";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(78, 197);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(13, 13);
			this->label14->TabIndex = 20;
			this->label14->Text = L"0";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(108, 304);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(13, 13);
			this->label15->TabIndex = 21;
			this->label15->Text = L"0";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(143, 373);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(13, 13);
			this->label16->TabIndex = 22;
			this->label16->Text = L"0";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(845, 110);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(13, 13);
			this->label17->TabIndex = 23;
			this->label17->Text = L"0";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(789, 198);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(13, 13);
			this->label18->TabIndex = 24;
			this->label18->Text = L"0";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(819, 304);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(13, 13);
			this->label19->TabIndex = 25;
			this->label19->Text = L"0";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(854, 374);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(13, 13);
			this->label20->TabIndex = 26;
			this->label20->Text = L"0";
			// 
			// Map
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSize = true;
			this->AutoValidate = System::Windows::Forms::AutoValidate::EnableAllowFocusChange;
			this->ClientSize = System::Drawing::Size(884, 611);
			this->Controls->Add(this->label20);
			this->Controls->Add(this->label19);
			this->Controls->Add(this->label18);
			this->Controls->Add(this->label17);
			this->Controls->Add(this->label16);
			this->Controls->Add(this->label15);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->cell_9_9);
			this->Controls->Add(this->cell_8_9);
			this->Controls->Add(this->cell_7_9);
			this->Controls->Add(this->cell_6_9);
			this->Controls->Add(this->cell_5_9);
			this->Controls->Add(this->cell_4_9);
			this->Controls->Add(this->cell_3_9);
			this->Controls->Add(this->cell_2_9);
			this->Controls->Add(this->cell_1_9);
			this->Controls->Add(this->cell_9_6);
			this->Controls->Add(this->cell_8_6);
			this->Controls->Add(this->cell_7_6);
			this->Controls->Add(this->cell_6_6);
			this->Controls->Add(this->cell_5_6);
			this->Controls->Add(this->cell_4_6);
			this->Controls->Add(this->cell_3_6);
			this->Controls->Add(this->cell_2_6);
			this->Controls->Add(this->cell_9_3);
			this->Controls->Add(this->cell_8_3);
			this->Controls->Add(this->cell_7_3);
			this->Controls->Add(this->cell_6_3);
			this->Controls->Add(this->cell_5_3);
			this->Controls->Add(this->cell_1_6);
			this->Controls->Add(this->cell_4_3);
			this->Controls->Add(this->cell_3_3);
			this->Controls->Add(this->cell_2_3);
			this->Controls->Add(this->cell_9_8);
			this->Controls->Add(this->cell_8_8);
			this->Controls->Add(this->cell_7_8);
			this->Controls->Add(this->cell_6_8);
			this->Controls->Add(this->cell_5_8);
			this->Controls->Add(this->cell_1_3);
			this->Controls->Add(this->cell_4_8);
			this->Controls->Add(this->cell_3_8);
			this->Controls->Add(this->cell_2_8);
			this->Controls->Add(this->cell_9_7);
			this->Controls->Add(this->cell_8_7);
			this->Controls->Add(this->cell_7_7);
			this->Controls->Add(this->cell_6_7);
			this->Controls->Add(this->cell_5_7);
			this->Controls->Add(this->cell_1_8);
			this->Controls->Add(this->cell_4_7);
			this->Controls->Add(this->cell_3_7);
			this->Controls->Add(this->cell_2_7);
			this->Controls->Add(this->cell_9_5);
			this->Controls->Add(this->cell_8_5);
			this->Controls->Add(this->cell_7_5);
			this->Controls->Add(this->cell_6_5);
			this->Controls->Add(this->cell_5_5);
			this->Controls->Add(this->cell_1_7);
			this->Controls->Add(this->cell_4_5);
			this->Controls->Add(this->cell_3_5);
			this->Controls->Add(this->cell_2_5);
			this->Controls->Add(this->cell_9_4);
			this->Controls->Add(this->cell_8_4);
			this->Controls->Add(this->cell_7_4);
			this->Controls->Add(this->cell_6_4);
			this->Controls->Add(this->cell_5_4);
			this->Controls->Add(this->cell_1_5);
			this->Controls->Add(this->cell_4_4);
			this->Controls->Add(this->cell_3_4);
			this->Controls->Add(this->cell_2_4);
			this->Controls->Add(this->cell_9_2);
			this->Controls->Add(this->cell_8_2);
			this->Controls->Add(this->cell_7_2);
			this->Controls->Add(this->cell_6_2);
			this->Controls->Add(this->cell_5_2);
			this->Controls->Add(this->cell_1_4);
			this->Controls->Add(this->cell_4_2);
			this->Controls->Add(this->cell_3_2);
			this->Controls->Add(this->cell_2_2);
			this->Controls->Add(this->cell_9_1);
			this->Controls->Add(this->cell_8_1);
			this->Controls->Add(this->cell_7_1);
			this->Controls->Add(this->cell_6_1);
			this->Controls->Add(this->cell_5_1);
			this->Controls->Add(this->cell_1_2);
			this->Controls->Add(this->cell_4_1);
			this->Controls->Add(this->cell_3_1);
			this->Controls->Add(this->cell_2_1);
			this->Controls->Add(this->cell_1_1);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(900, 650);
			this->MinimumSize = System::Drawing::Size(900, 650);
			this->Name = L"Map";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Two Towers(Pre-Alpha)";
			this->Load += gcnew System::EventHandler(this, &Map::Map_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_1_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_2_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_3_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_5_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_6_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_7_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_8_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_9_9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cell_4_5))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

void checkMap()
{
	ofstream fout;
	fout.open("Map.txt", ios::out);

	fout << "Mount" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			fout << map[i][j].isMount << ' ';
		}
		fout << endl;
	}
	
	fout << endl << "Ruins" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			fout << map[i][j].isRuin << ' ';
		}
		fout << endl;
	}

	fout << endl << "Player" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			fout << map[i][j].player << ' ';
		}
		fout << endl;
	}

	fout << endl << "Workers" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			fout << map[i][j].worker << ' ';
		}
		fout << endl;
	}

	fout << endl << "Barracks" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			fout << map[i][j].barrack << ' ';
		}
		fout << endl;
	}

	fout << endl << "Towers" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			fout << map[i][j].tower << ' ';
		}
		fout << endl;
	}

	fout << endl << "Defense" << endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			fout << map[i][j].defense << ' ';
		}
		fout << endl;
	}

	fout.close();
}

private: System::Void cell_1_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_1_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_2_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_3_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_4_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_5_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_6_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_7_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_8_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_5_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_7_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_8_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
private: System::Void cell_9_9_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!Open && !OpenOpt)
			 {
				 Commands^ gCommands = gcnew Commands;
				 gCommands->Show();
				 Open = true;
			 }
}
public: System::Void Map_Load(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!OpenOpt && !Open)
			 {
				 Options^ gOptions = gcnew Options;
				 gOptions->Show();
				 OpenOpt = true;
			 }
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	generateMap();
	generateMount();
	generateRuins();
	checkMap();
}
};
}